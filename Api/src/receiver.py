import mysql.connector
import json
from numpy import average
import pandas as pd
import os


def load_configuration(filepath=None):
    filepath = filepath or os.path.dirname(__file__) + "/configs/configurations.json"
    with open(filepath, "r") as f:
        config = json.load(f)
    return config


def db_con(db_config):
    connection = mysql.connector.connect(
        host=db_config["host"],
        database=db_config["db_name"],
        user=db_config["user"],
        password=db_config["password"],
        port=db_config["port"],
    )
    if connection.is_connected():
        print("Succesfully connected to database")
        return connection
    else:
        print("Cannot connect do database")


def select_from_db(connector, incubator_number, date):
    sql = prepare_sql(incubator_number, date)
    cursor = connector.cursor()
    cursor.execute(sql)
    myresult = cursor.fetchall()
    return myresult


def prepare_sql(incubator_number, date):
    sql = (
        "SELECT AVG(temperature) FROM inkubator WHERE  date BETWEEN '"
        + date
        + " 00:00:00' AND '"
        + date
        + " 23:59:59' AND incubator_number = "
        + incubator_number
    )
    return sql


def get_average(incubator_number, date):
    config = load_configuration()
    connector = db_con(config["database"])
    daily_average = select_from_db(connector, incubator_number, date)
    connector.close()
    return daily_average
