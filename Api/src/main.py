import json
from flask import Flask
from flask_restful import Resource, Api
import receiver
import sender


app = Flask(__name__)
api = Api(app)


class insert_data(Resource):
    def get(self, incubator_number, temperature):
        return sender.insert_data(incubator_number, temperature)


class get_average(Resource):
    def get(self, incubator_number, date):
        return receiver.get_average(incubator_number, date)


class Main(Resource):
    def get(self):
        return "..."


api.add_resource(insert_data, "/insert_data/<incubator_number>/<temperature>")
api.add_resource(get_average, "/get_average/<incubator_number>/<date>")
api.add_resource(Main, "/")

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
